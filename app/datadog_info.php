<?php

$datadogTracerBootstrapClass = DDTrace\Bootstrap::class;
$datadogTracingEnabled = extension_loaded('ddtrace') && function_exists('DDTrace\Bridge\dd_tracing_enabled') && DDTrace\Bridge\dd_tracing_enabled();
$datadogTracerLoaderRegistered = class_exists($datadogTracerBootstrapClass);
$datadogTracerInitialized = 0;
if ($datadogTracerLoaderRegistered) {
    $datadogTracerInitializedFlag = new ReflectionProperty($datadogTracerBootstrapClass, 'bootstrapped');
    $datadogTracerInitializedFlag->setAccessible(true);
    $datadogTracerInitialized = (int) $datadogTracerInitializedFlag->getValue(null);
}

echo json_encode(
    [
        'Datadog extension_loaded' => (int) extension_loaded('ddtrace'),
        'Datadog extension version' => phpversion('ddtrace'),
        'Tracing enabled by configuration' => (int) $datadogTracingEnabled,
        'Tracer classes are loadable' => (int) $datadogTracerLoaderRegistered,
        'Tracer is initialized' => $datadogTracerInitialized,
    ],
    JSON_PRETTY_PRINT
), PHP_EOL;
