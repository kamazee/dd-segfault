<?php

require __DIR__ . '/vendor/autoload.php';

require __DIR__ . '/datadog_info.php';

const ITERATION_COUNT = 10000;

$options = [
    'redisDSN' => 'tcp://host.docker.internal:6379?database=0'
];
$results2 = [];
for ($i = 0; $i < ITERATION_COUNT; $i++) {
    $redis = new \Predis\Client($options['redisDSN']);
    $start = microtime(true);

    $tmpTtl = 2;
    $tmpKey = 'tmp-key:' . $i;
    $tmpValue = 'tmp-value-' . $i;
    $redis->setex($tmpKey, $tmpTtl, $tmpValue);
    if ($tmpValue !== $redis->get($tmpKey)) {
        throw new \Exception('Redis get error', 1);
    }
    if (!$redis->del($tmpKey)) {
        throw new \Exception('Redis delete error', 1);
    }

    $results2[] = microtime(true) - $start;
    echo sprintf('%04d %.3f', $i, memory_get_usage(true) / (1024 * 1024)), PHP_EOL;
}
echo 'Redis benchmark: '
    . 'Iterations = ' . count($results2) . '; average = ' . array_sum($results2) / count($results2)
    . '; memory usage = '. memory_get_usage(true) / (1024 * 1024) . 'Mb' . PHP_EOL;
