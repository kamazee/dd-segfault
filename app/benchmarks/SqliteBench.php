<?php

use PhpBench\Benchmark\Metadata\Annotations\AfterMethods;
use PhpBench\Benchmark\Metadata\Annotations\BeforeClassMethods;
use PhpBench\Benchmark\Metadata\Annotations\BeforeMethods;
use PhpBench\Benchmark\Metadata\Annotations\Iterations;
use PhpBench\Benchmark\Metadata\Annotations\Revs;

/**
 * @BeforeClassMethods({"printDatadogStatus"})
 * @BeforeMethods({"setUp"})
 * @AfterMethods({"shutDown"})
 */
class SqliteBench
{
    private const DB_PATH = '/mnt/database.sqlite';

    private $pdo;

    private $counter;

    public static function printDatadogStatus()
    {
//        require __DIR__ . '/../datadog_status.php';
        return json_encode(["datadog" => 1]);
    }

    /**
     * @Revs(10000)
     * @Iterations(5)
     */
    public function bench()
    {
        $rem = $this->counter % 100;
        $statement = $this->pdo->query(sprintf('SELECT * FROM performance_test WHERE id = %d', $rem));
        $statement->execute();
        $result = $statement->fetchAll();
        assert($result['name'] === "Name $rem");
        ++$this->counter;
    }

    public function setUp()
    {
        $this->counter = 0;
        $this->pdo = new PDO('sqlite:' . self::DB_PATH);
        $this->pdo->exec('CREATE TABLE performance_test (id INT PRIMARY KEY, name TEXT)');
        for ($i = 0; $i < 100; $i++) {
            $this->pdo->exec("INSERT INTO performance_test (id, name) VALUES ({$i}, 'Name {$i}')");
        }
    }

    public function shutDown()
    {
        assert($this->pdo instanceof PDO);
        $this->pdo = null;
        unlink(self::DB_PATH);
    }
}
