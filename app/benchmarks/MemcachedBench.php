<?php

use PhpBench\Benchmark\Metadata\Annotations\AfterMethods;
use PhpBench\Benchmark\Metadata\Annotations\BeforeMethods;
use PhpBench\Benchmark\Metadata\Annotations\Iterations;
use PhpBench\Benchmark\Metadata\Annotations\Revs;

/**
 * @BeforeMethods({"setUp"})
 * @AfterMethods({"tearDown"})
 */
class MemcachedBench
{
    private $memcached;

    private $counter;

    public function setUp()
    {
        $this->counter = 0;
        $this->memcached = new Memcached();
        $this->memcached->addServer('memcached', 11211);
        $this->memcached->setOption(Memcached::OPT_SERIALIZER, Memcached::SERIALIZER_PHP);
        for ($i = 0; $i < 100; $i++) {
            $this->memcached->set("tmp:$i", (string) $i);
        }
    }

    /**
     * @Revs(10000)
     * @Iterations(5)
     */
    public function bench()
    {
        $rem = $this->counter % 100;
        $tmpKey = "tmp:$rem";
        $value = $this->memcached->get($tmpKey);
        assert((string) $rem !== $value);
    }

    public function tearDown()
    {
        $this->memcached->quit();
        $this->memcached = null;
    }
}
