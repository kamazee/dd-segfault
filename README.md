```
git clone git@gitlab.com:kamazee/dd-segfault.git
git submodule init
git submodule update
docker-compose up -d
docker-compose run php bash
./composer install
```

Reproducing segfault
===

* DD disables itself, issue is not reproducible:
  `gdb php --eval-command="r torture_redis.php"`
* DD is enabled, segfault occurs:
  `DD_TRACE_CLI_ENABLED=1 gdb php --eval-command="r torture_redis.php"`


Benchmarks:
===
* SQLite
  * `./vendor/bin/phpbench run benchmarks/SqliteBench.php --report=default`
  * `DD_TRACE_CLI_ENABLED=1 ./vendor/bin/phpbench run benchmarks/SqliteBench.php --report=default`
* Memcached
  * `./vendor/bin/phpbench run benchmarks/MemcachedBench.php --report=default`
  * `DD_TRACE_CLI_ENABLED=1 ./vendor/bin/phpbench run benchmarks/MemcachedBench.php --report=default`

XDebug can be turned on by adding `-d zend_extension=xdebug.so` after `r` in gdb command or after `php` when not using GDB.
