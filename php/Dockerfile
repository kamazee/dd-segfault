FROM ubuntu:16.04

ENV LC_ALL=C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get install -y software-properties-common apt-utils
RUN add-apt-repository ppa:ondrej/php
RUN echo "apt cache version 1" > /tmp/version.txt
RUN apt-get update
# Unless php7.1-common php7.1-json php7.1-xml are explicitly listed, their php7.3-* siblings are installed
RUN apt-get install -y php7.1-cli php7.1-dev php7.1-common php7.1-json php7.1-xml

RUN mkdir /srv/dd-repro
COPY dd-trace-php /srv/dd-repro/dd-trace-php

# Implicit dependency of datadog tracer
RUN apt-get install -y libcurl4-openssl-dev
WORKDIR /srv/dd-repro/dd-trace-php
RUN phpize
RUN make

RUN sed -i 's@main$@main main/debug@' /etc/apt/sources.list.d/ondrej-ubuntu-php-xenial.list
RUN apt-get update
RUN apt-get install -y php7.1-cli-dbgsym php7.1-common-dbgsym
RUN apt-get install -y gdb
RUN apt-get install -y less libc6-dbg cyrus-sasl2-dbg

RUN mkdir -p /build/php7.1-JdcYxT
WORKDIR /build/php7.1-JdcYxT
RUN curl --location --remote-name 'https://www.php.net/distributions/php-7.1.30.tar.bz2'
RUN mkdir php7.1-7.1.30
RUN tar -C php7.1-7.1.30 -xf php-7.1.30.tar.bz2 --strip-components=1 php-7.1.30/
RUN echo 'set auto-load safe-path /' > /root/.gdbinit

RUN apt-get install -y php-xdebug php-xdebug-dbgsym
COPY xdebug.ini /etc/php/7.1/mods-available/xdebug.ini
COPY dd.ini /etc/php/7.1/mods-available/dd.ini
RUN phpenmod -s cli -v 7.1 dd
RUN apt-get install -y unzip
RUN apt-get install -y php7.1-sqlite3 php7.1-curl
RUN apt-get install -y php-memcached

WORKDIR /srv/dd-repro/app
